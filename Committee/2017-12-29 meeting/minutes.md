# Meeting minutes 2017-12-29

Present:

* Jakub Václavovič
* Christine Cutajar
* Robert Cutajar (organizer and meeting chairman)

Topics:

1. Budgetting and acounting year will start every 1st September. The first year starts on 1st January 2018 and ends by the end of August 2018.
2. [Kids club budget 2018](kids%20club%20budget%202018.pdf) has been approved,
3. Employment for kids club
   - We shall employ three employees to run the project: Bani, Jo, Iva
   - We agree to pay minimum salary as provided by law
   - We agree to make use of the work time account as provided by law
4. Membership fees - the committee gives the chairman a frame for setting fees
   - Kids club
     - We'll keep the fees of Jeff and Debi for the kids club membership.
     - See the [kids club fees](kids%20club%20fees.pdf).
   - Accommodation for members
     - The chairman can decide to provide accommodation to members.
     - Fees should be proportional to the actual costs per person.
     - Fees can be waived or reduced on merit of member participation.